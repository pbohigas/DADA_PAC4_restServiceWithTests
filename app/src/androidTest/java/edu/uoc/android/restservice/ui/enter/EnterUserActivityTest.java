package edu.uoc.android.restservice.ui.enter;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import edu.uoc.android.restservice.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EnterUserActivityTest {

    private static String userName = "yoshuawuyts";
    private static String knownFollowerName = "afroald";

    @Rule
    public ActivityTestRule<EnterUserActivity> mActivityTestRule = new ActivityTestRule<>(EnterUserActivity.class);

    @Test
    public void enterUserActivityTest() throws Exception {

        // Type user name into input field
        onView(withId(R.id.enter_user_edit_text)).perform(typeText(userName), closeSoftKeyboard());

        // Press the button
        onView(withId(R.id.enter_user_button)).perform(click());

        // Wait till the followers are loaded
        Thread.sleep(2000);

        // Scroll to the position that matches with the known follower (if there isn't returns error)
        onView(withId(R.id.user_followers_recycler_view))
                .perform(scrollTo(hasDescendant(withText(knownFollowerName))));
    }

}


